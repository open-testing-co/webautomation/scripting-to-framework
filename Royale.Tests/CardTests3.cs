using System.Collections.Generic;
using Framework.Models;
using Framework.Selenium;
using Framework.Services;
using NUnit.Framework;
using Royale.Pages;

namespace Royale.Tests
{
    public class CardTests3
    {

        [SetUp]
        public void BeforeEach()
        {
            Driver.Init();
            Page.Init();
            Driver.Goto("https://statsroyale.com/es");
        }

        [TearDown]
        public void AfterEach()
        {
            Driver.Quit();
        }

        [Test]
        [Parallelizable()]
        public void Ice_Spirit_is_on_Cards_Page()
        {
            var iceSpirit = Page.Cards.Goto().GetCardByName("Ice Spirit");
            Assert.That(iceSpirit.Displayed);
        }

        [Test]
        [Parallelizable()]
        public void Ice_Spirit_headers_are_correct_on_Cards_Details_Page()
        {
            Page.Cards.Goto().GetCardByName("Ice Spirit").Click();

            var (category, arena) = Page.CardDetails.GetCardCategory();
            var cardname = Page.CardDetails.Map.CardName.Text;
            var cardCommon = Page.CardDetails.Map.CardRarity.Text;
            Assert.AreEqual("Espíritu de hielo", cardname);
            Assert.AreEqual("Tropa", category);
            Assert.AreEqual("Arena 8", arena);
            StringAssert.Contains("Común", cardCommon);
        }

        [Test]
        [Parallelizable()]
        public void Mirror_headers_are_correct_on_Cards_Details_Page()
        {
            Page.Cards.Goto().GetCardByName("Mirror").Click();

            var (category, arena) = Page.CardDetails.GetCardCategory();
            var cardname = Page.CardDetails.Map.CardName.Text;
            var cardCommon = Page.CardDetails.Map.CardRarity.Text;
            Assert.AreEqual("Espejo", cardname);
            Assert.AreEqual("Hechizo", category);
            Assert.AreEqual("Arena 12", arena);
            StringAssert.Contains("Épica", cardCommon);
        }

        [Test]
        [Parallelizable()]
        public void Mirror_headers_are_correct_on_Cards_Details_Page2()
        {
            Page.Cards.Goto().GetCardByName("Mirror").Click();
            var card = Page.CardDetails.GetBaseCard();
            var mirror = new MirrorCard();

            Assert.AreEqual(mirror.Name, card.Name);
            Assert.AreEqual(mirror.Type, card.Type);
            Assert.AreEqual(mirror.Arena, card.Arena);
            StringAssert.Contains(mirror.Rarity, card.Rarity);
        }

        [Test]
        [TestCase("Ice Spirit")]
        [TestCase("Mirror")]
        [Parallelizable()]
        public void Card_headers_are_correct_on_Cards_Details_Page3(string cardName)
        {
            Page.Cards.Goto().GetCardByName(cardName).Click();
            var cardOnPage = Page.CardDetails.GetBaseCard();
            var cardExpected = new InMemoryCardService().GetCardByName(cardName);

            Assert.AreEqual(cardExpected.Name, cardOnPage.Name);
            Assert.AreEqual(cardExpected.Type, cardOnPage.Type);
            Assert.AreEqual(cardExpected.Arena, cardOnPage.Arena);
            StringAssert.Contains(cardExpected.Rarity, cardOnPage.Rarity);
        }

        static string[] cardNames = { "Ice Spirit", "Mirror" };
        [Test, Category("cards")]
        [TestCaseSource("cardNames")]
        [Parallelizable(ParallelScope.Children)]
        public void Card_headers_are_correct_on_Cards_Details_Page4(string cardName)
        {
            Page.Cards.Goto().GetCardByName(cardName).Click();
            var cardOnPage = Page.CardDetails.GetBaseCard();
            var cardExpected = new InMemoryCardService().GetCardByName(cardName);

            Assert.AreEqual(cardExpected.Name, cardOnPage.Name);
            Assert.AreEqual(cardExpected.Type, cardOnPage.Type);
            Assert.AreEqual(cardExpected.Arena, cardOnPage.Arena);
            StringAssert.Contains(cardExpected.Rarity, cardOnPage.Rarity);
        }

        [Test]
        [Parallelizable()]
        public void Ice_Spirit_is_on_Cards_Page5()
        {
            var iceSpirit = Page.Cards.Goto().GetCardByName("Ice Spirit");
            Assert.That(iceSpirit.Displayed);
        }


        static IList<Card> apiCards = new ApiCardService().GetAllCards();
        [Test]
        [TestCaseSourceAttribute("apiCards")]
        [Parallelizable(ParallelScope.Children)]
        public void Ice_Spirit_is_on_Cards_Page6(Card card)
        {
            var cardOnPage = Page.Cards.Goto().GetCardByName(card.Name);
            Assert.That(cardOnPage.Displayed);
        }
        [Test, Category("cards")]
        [TestCaseSource("apiCards")]
        [Parallelizable(ParallelScope.Children)]
        public void Card_headers_are_correct_on_Cards_Details_Page7(Card card)
        {
            Page.Cards.Goto().GetCardByName(card.Name).Click();
            var cardOnPage = Page.CardDetails.GetBaseCard();

            Assert.AreEqual(card.Name, cardOnPage.Name);
            Assert.AreEqual(card.Type, cardOnPage.Type);
            Assert.AreEqual(card.Arena, cardOnPage.Arena);
            StringAssert.Contains(card.Rarity, cardOnPage.Rarity);
        }
    }
}