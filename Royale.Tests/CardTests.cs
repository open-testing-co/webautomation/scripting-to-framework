using System.IO;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Royale.Tests
{
    public class CardTests
    {
        IWebDriver driver;

        [SetUp]
        public void BeforeEach()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--start-maximized");

            var chrome_path =Path.GetFullPath(@"../../../../"+"_drivers");
            driver= new ChromeDriver(chrome_path, options);

            driver.Manage().Timeouts().ImplicitWait = System.TimeSpan.FromMilliseconds(30000);
            driver.Manage().Timeouts().PageLoad = System.TimeSpan.FromMilliseconds(400000);
            driver.Manage().Timeouts().AsynchronousJavaScript = System.TimeSpan.FromMilliseconds(60000);
        }

        [TearDown]
        public void AfterEach()
        {
            driver.Quit();
        }

        [Test]
        public void Ice_Spirit_is_on_Cards_Page()
        {
            driver.Url="https://statsroyale.com/es";
            driver.FindElement(By.CssSelector("a[href='/es/cards']")).Click();
            var iceSpirit = driver.FindElement(By.CssSelector("a[href*='Ice+Spirit']"));
            Assert.That(iceSpirit.Displayed);
        }

        [Test]
        public void Ice_Spirit_headers_are_correct_on_Cards_Details_Page()
        {
            driver.Url="https://statsroyale.com/es";
            driver.FindElement(By.CssSelector("a[href='/es/cards']")).Click();
            driver.FindElement(By.CssSelector("a[href*='Ice+Spirit']")).Click();
            var cardname=driver.FindElement(By.CssSelector("[class*='cardName']")).Text;
            var cardCategory=driver.FindElement(By.CssSelector(".card__rarity")).Text.Split(", ");
            var cardType= cardCategory[0];
            var cardArnea= cardCategory[1];
            var cardCommon=driver.FindElement(By.ClassName("card__common")).Text;
            Assert.AreEqual("Espíritu de hielo",cardname);
            Assert.AreEqual("Tropa",cardType);
            Assert.AreEqual("Arena 8",cardArnea);
            Assert.AreEqual("Común",cardCommon);
        }
    }
}