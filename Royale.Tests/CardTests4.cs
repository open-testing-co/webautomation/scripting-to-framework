using System.Collections.Generic;
using Framework.Models;
using Framework.Services;
using NUnit.Framework;
using Royale.Pages;
using Royale.Tests.Base;

namespace Royale.Tests
{
    public class CardTests4:TestBase
    {
        

        static string[] cardNames = { "Ice Spirit", "Mirror" };
        [Test, Category("cards")]
        [TestCaseSource("cardNames")]
        [Parallelizable(ParallelScope.Children)]
        public void Card_headers_are_correct_on_Cards_Details_Page4(string cardName)
        {
            Page.Cards.Goto().GetCardByName(cardName).Click();
            var cardOnPage = Page.CardDetails.GetBaseCard();
            var cardExpected = new InMemoryCardService().GetCardByName(cardName);

            Assert.AreEqual(cardExpected.Name, cardOnPage.Name);
            Assert.AreEqual(cardExpected.Type, cardOnPage.Type);
            Assert.AreEqual(cardExpected.Arena, cardOnPage.Arena);
            StringAssert.Contains(cardExpected.Rarity, cardOnPage.Rarity);
        }

        static IList<Card> apiCards = new ApiCardService().GetAllCards();
        [Test]
        [TestCaseSourceAttribute("apiCards")]
        [Parallelizable(ParallelScope.Children)]
        public void Ice_Spirit_is_on_Cards_Page6(Card card)
        {
            var cardOnPage = Page.Cards.Goto().GetCardByName(card.Name);
            Assert.That(cardOnPage.Displayed);
        }
        [Test, Category("cards")]
        [TestCaseSource("apiCards")]
        [Parallelizable(ParallelScope.Children)]
        public void Card_headers_are_correct_on_Cards_Details_Page7(Card card)
        {
            Page.Cards.Goto().GetCardByName(card.Name).Click();
            var cardOnPage = Page.CardDetails.GetBaseCard();

            Assert.AreEqual(card.Name, cardOnPage.Name);
            Assert.AreEqual(card.Type, cardOnPage.Type);
            Assert.AreEqual(card.Arena, cardOnPage.Arena);
            StringAssert.Contains(card.Rarity, cardOnPage.Rarity);
        }
    }
}