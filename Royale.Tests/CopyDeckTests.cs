﻿using Framework.Selenium;
using NUnit.Framework;
using Royale.Pages;
using Royale.Tests.Base;

namespace Royale.Tests
{
    public class CopyDeckTests:TestBase
    {
        [Test]
        public void User_can_copy_the_deck()
        {
            Page.DeckBuilder.Goto().AddCardsManually();
            Page.DeckBuilder.CopySuggestedDeck();
            Page.CopyDeck.Yes();
            
            Assert.That(Page.CopyDeck.Map.CopiedMessage.Displayed);
        }

        [Test, Category("copydeck")]
        [Parallelizable()]
        public void User_opens_app_store()
        {
            Page.DeckBuilder.Goto().AddCardsManually();
            Page.DeckBuilder.CopySuggestedDeck();
            Page.CopyDeck.No().OpenAppStore();

            //Assert.That(Driver.Title, Is.EqualTo("Clash Royale on the App Store"));
            StringAssert.Contains("‎Clash Royale on the App Store", Driver.Title);
        }

        [Test, Category("copydeck")]
        [Parallelizable()]
        public void User_opens_google_play()
        {
            Page.DeckBuilder.Goto().AddCardsManually();
            Page.DeckBuilder.CopySuggestedDeck();
            Page.CopyDeck.No().OpenGooglePlay();

            //Assert.AreEqual("Clah Royale - Apps on Google Play", Driver.Title);
            StringAssert.Contains("Clash Royale - Apps on Google Play", Driver.Title);
        }
    }
}
