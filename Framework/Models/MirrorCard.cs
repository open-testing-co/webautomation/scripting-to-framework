﻿namespace Framework.Models
{
    public class MirrorCard:Card
    {
        public override string Name { get; set; } = "Espejo";
        public override int Cost { get; set; } = 1;
        public override string Rarity { get; set; } = "Épica";
        public override string Type { get; set; } = "Hechizo";
        public override string Arena { get; set; } = "Arena 12";
    }
}
