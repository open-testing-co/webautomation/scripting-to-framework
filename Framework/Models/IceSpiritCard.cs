﻿namespace Framework.Models
{
    public class IceSpiritCard:Card
    {
        public override string Name { get; set; } = "Espíritu de hielo";
        public override int Cost { get; set; } = 1;
        public override string Rarity { get; set; } = "Común";
        public override string Type { get; set; } = "Tropa";
        public override string Arena { get; set; } = "Arena 8";
    }
}
