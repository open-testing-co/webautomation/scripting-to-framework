﻿using Framework.Models;

namespace Framework.Services
{
    public class InMemoryCardService : ICardService
    {
        public Card GetCardByName(string cardName)
        {
            return cardName switch
            {
                "Ice Spirit" => new IceSpiritCard(),
                "Mirror" => new MirrorCard(),
                _ => throw new System.ArgumentException("Card is no available : " + cardName),
            };
        }
    }
}
