﻿using Framework.Selenium;
using OpenQA.Selenium;

namespace Royale.Pages
{
    public class CopyDeckPage
    {
        public readonly CopyDeckPageMap Map;

        public CopyDeckPage()
        {
            Map = new CopyDeckPageMap();
        }

        public void Yes()
        {
            //Driver.Wait.Until(drvr => Map.AcceptCookies.Displayed);
            Driver.Wait.Until(WaitConditions.ElementDisplayed(Map.AcceptCookies));
            Map.AcceptCookies.Click();
            //Driver.Wait.Until(drvr => Map.YesButton.Displayed);
            Driver.Wait.Until(WaitConditions.ElementDisplayed(Map.YesButton));
            Map.YesButton.Click();
        }

        public CopyDeckPage No()
        {
            //Driver.Wait.Until(drvr => Map.AcceptCookies.Displayed);
            Driver.Wait.Until(WaitConditions.ElementDisplayed(Map.AcceptCookies));
            Map.AcceptCookies.Click();
            //Driver.Wait.Until(drvr => Map.YesButton.Displayed);
            Driver.Wait.Until(WaitConditions.ElementDisplayed(Map.YesButton));
            Map.NoButton.Click();
            return this;
        }

        public void OpenGooglePlay()
        {
            //Driver.Wait.Until(drvr => Map.GooglePlayDownload.Displayed);
            Driver.Wait.Until(WaitConditions.ElementDisplayed(Map.GooglePlayDownload));
            Map.GooglePlayDownload.Click();
        }

        public void OpenAppStore()
        {
            //Driver.Wait.Until(drvr => Map.AppStoreDownload.Displayed);
            Driver.Wait.Until(WaitConditions.ElementDisplayed(Map.AppStoreDownload));
            Map.AppStoreDownload.Click();
        }
    }

    public class CopyDeckPageMap
    {
        public Element YesButton => Driver.FindElement(By.Id("button-open"),"Yes Button");
        public Element NoButton => Driver.FindElement(By.Id("not-installed"), "No Button");
        public Element CopiedMessage => Driver.FindElement(By.CssSelector(".notes.active"), "Copied Message");
        public Element AcceptCookies => Driver.FindElement(By.CssSelector(".cc-btn.cc-dismiss"), "Accept Cookies");
        public Element AppStoreDownload => Driver.FindElement(By.CssSelector(".os-ios.track-download"),"App Store Download");
        public Element GooglePlayDownload => Driver.FindElement(By.CssSelector(".os-android.track-download"), "Google Play Download");
    }
}
