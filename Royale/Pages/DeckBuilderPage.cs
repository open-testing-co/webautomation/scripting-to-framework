﻿using Framework.Selenium;
using OpenQA.Selenium;

namespace Royale.Pages
{
    public class DeckBuilderPage : PageBase
    {
        public readonly DeckBuilderPageMap Map;

        public DeckBuilderPage()
        {
            Map = new DeckBuilderPageMap();
        }

        public DeckBuilderPage Goto()
        {
            HeaderNav.Map.DeckBuilderLink.Click();
            return this;
        }

        public void AddCardsManually()
        {
            //Driver.Wait.Until(drvr => Map.AddCardsManuallyLink.Displayed);
            Driver.Wait.Until(WaitConditions.ElementDisplayed (Map.AddCardsManuallyLink));
            Map.AddCardsManuallyLink.Click();
        }

        public void CopySuggestedDeck()
        {
            //Driver.Wait.Until(drvr => Map.CopyDeckIcon.Displayed);
            Driver.Wait.Until(WaitConditions.ElementDisplayed(Map.AddCardsManuallyLink));
            Map.CopyDeckIcon.Click();
        }

    }

    public class DeckBuilderPageMap
    {
        public Element AddCardsManuallyLink => Driver.FindElement(By.XPath("//a[text()='add cards manually']"), "Add Cards Manually Link");
        public Element CopyDeckIcon => Driver.FindElement(By.CssSelector(".copyButton"), "Copy Desk Icon");
    }
}
